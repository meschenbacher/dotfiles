# dotfiles

Collection of dotfiles. The README highlights a few important points.

The repo itself contains the [install.sh](install.sh) script which reads a few lists of files
(`files`, `files-opt`, `$HOSTNAME`, `$HOSTNAME-not`) and symlinks or copies files from the
repo to the home directory.

# Deal with proxy environments

Using ssh within a proxy environment becomes a challenge if your workstation only *sometimes*
requires a proxy for outside access. Reconfiguring `~/.ssh/config` several times a day may
become frustrating. This section describes a way to dynamically decide if ssh should use
`ProxyCommand=corkscrew ...` or direct connection without `ProxyCommand`.

## Requirements

`bin/bracketize_ipv6.py` needs `python3-netaddr`.
The ssh-proxy-decision scripts need `zsh`.

# Walkthrough

Include three scripts into a folder where `$PATH` points to (i.e. add them to `files`):
- `bin/bracketize_ipv6.py`
- `bin/ssh_decide_corkscrew.py`
- `bin/ssh_corkscrew.py`

Configure a default proxy (to be used by the ssh-via-proxy scripts):

```
export default_http_proxy=http://proxy.example.org:80
export default_https_proxy=$default_http_proxy
export default_no_proxy="localhost,127.0.0.1,example.org"
```

Configure the ssh client via `ssh_config` e.g. `~/.ssh/config` by placing the snippet towards
the end of the configuration file:

```
Match Host * exec "ssh_decide_corkscrew.sh '%h' %p"
	# proxies usually don't like idle connections...
	ServerAliveInterval 25
	ProxyCommand ssh_corkscrew.sh "%h" "%p"
```

Add some convenience aliases to either let our scripts decide or to manually overwrite the
decision

```
alias s=ssh
alias sshp="SSH_ALWAYS_PROXY=yes ssh"
alias sshn="SSH_NO_PROXY=yes ssh"
```
