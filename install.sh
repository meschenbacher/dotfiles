#!/usr/bin/env bash

set -o nounset -o errexit -o pipefail

# based on https://github.com/maxfragg/dotfiles and
# https://github.com/thoughtbot/dotfiles
# read content of files to decide, which files should be installed
# files contains files for all hosts
#
# if filename starts with a "#" it will not be treated as hidden,
# and no "." will be prepended
#
# if filename starts with a "!" it will be copied instead of symlinked
#
# format [!][#]target_filename_without_dot[=source]
#
# this script should never overwrite files without asking (except for updating
# or removing outdated symlinks)
#
# $HNAME contains host specific files
# $HNAME-not contains files excluded for this host, overwrites everything else
#
# if the target is already a symlink, it will be updated to point to
# the new location

cd "$(dirname "$0")" || (echo could not chdir to the script >/dev/stderr; exit 1)

# implement your logic of grouping hosts into custom $HNAME sections

# which_host sets $HNAME based on the $HOSTNAME of the host
case "$HOSTNAME" in
	hel | kolga | magni | equinox | skadi | munin | bums-beta | www | tuerserver | unifi | proxy | vpn | monitoring)
		HNAME="sh"
		;;
	faui0*)
		HNAME="cip"
		;;
	work*)
		HNAME="work"
		;;
	*)
		HNAME="$HOSTNAME"
		;;
esac

# TODO create ~/bin ~/.config/qutebrowser

# do not modify lines below this line

DOTFILES="$(cat files)"

# 0 is okay
# 1 indicates any warning occured
EXITCODE=0

if [ -f "$HNAME" ]; then
	DOTFILES="$DOTFILES $(cat "$HNAME")"
fi

if [ -f "$HNAME"-not ]; then
	if [ -f "$HNAME" ]; then
		DOTFILES=$(comm -3 <(cat files "$HNAME"|sort) <(sort "$HNAME"-not))
	else
		DOTFILES=$(comm -3 <(sort "files") <(sort "$HNAME"-not))
	fi
fi

if [ -f custom.sh ]; then
	# ask to install custom.sh
	echo $ cat custom.sh
	cat custom.sh
	echo "Install custom.sh? [y/N]"
	read -r answer
	if [[ $answer == "y" ]]; then
		bash custom.sh
	else
		echo Not running custom.sh
	fi
fi

if [ -f files-opt ]; then
	for name in $(cat files-opt); do
		echo "Install $name? [y/N]"
		read -r answer
		if [[ $answer == "y" ]]; then
			echo "adding" "$name"
			DOTFILES="$DOTFILES $name"
		else
			echo "skipping" "$name"
		fi
	done
fi

for line in $DOTFILES; do

	binargs="ln -s"
	# binargs=echo

	# split line by =
	arr=( ${line//=/ } )
	name=$(echo "${arr[0]}" | envsubst)
	if [ ! -z "${arr[1]+x}" ]; then
		altname=$(echo "${arr[1]}" | envsubst)
	else
		unset altname
	fi

	hashi=0
	if expr index "$name" "#">/dev/null; then
		hashi=$(expr index "$name" "#")
	fi
	exlami=0
	if expr index "$name" "!">/dev/null; then
		exlami=$(expr index "$name" "!")
	fi
	# ishash=false
	isexclam=false

	# dot at the beginning
	dot=.

	# handle case with a hashtag (no dot at the beginning of target)
	if [[ $hashi -gt 0 ]] && [[ $hashi -lt 3 ]]; then
		# ishash=true
		name=${name/\#/}
		dot=""
	fi

	# handle case with exclamation mark (no symlink but cp)
	if [[ $exlami -gt 0 ]] && [[ $exlami -lt 3 ]]; then
		isexclam=true
		name=${name/\!/}
		name=$(echo "$name" | envsubst)
		binargs="cp -r"
	fi

	target="$HOME/$dot$name"

	#if [ -e "$target" ] ||
	if [ $isexclam = true ]; then
		if [ -e "$target" ] && [ -L "$target" ]; then
			echo "UPDATING: $target from symlink to hardcopy (old symlink pointed to $(readlink "$target")"
			rm "$target"
			$binargs "$PWD/${altname:-$name}" "$target"
		elif [ -e "$target" ]; then
			# check for differences
			r=$(diff -u "$PWD/$altname" "$target") > /dev/null
			# shellcheck disable=SC2181
			if [[ $? -ne 0 ]]; then
				echo "WARNING: $name exists but has a diff!"
				EXITCODE=1
				echo "$r"
			else
				echo "OK: $target"
			fi
		else
			# target does not exist
			echo "Creating $target"
			$binargs "$PWD/${altname:-$name}" "$target"
		fi
	elif [ -e "$target" ] || [ -L "$target" ]; then
		# if the target exists but is a symlink, replace the symlink
		if [ -L "$target" ]; then
			oldtarget=$(readlink "$target")
			if [ "$oldtarget" != "$PWD/${altname:-$name}" ]; then
				echo "UPDATING: $target (old symlink pointed to $oldtarget)"
				rm "$target"
				$binargs "$PWD/${altname:-$name}" "$target"
			else
				echo "OK: $target"
			fi
		else
			echo "WARNING: $target exists but is not a symlink."
			EXITCODE=1
		fi
	else
		echo "Creating $target"
		$binargs "$PWD/${altname:-$name}" "$target"
	fi
done

exit $EXITCODE
