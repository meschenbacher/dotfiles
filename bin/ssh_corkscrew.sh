#!/usr/bin/env zsh

set -o errexit -o pipefail -o nounset

host=$1
port=$2
local_https_proxy=${https_proxy:-$default_https_proxy}
local_https_proxy=${local_https_proxy/http:\/\//}
proxyarr=("${(@s/:/)${local_https_proxy}}")
proxy=$proxyarr[1]
proxyport=$proxyarr[2]

exec corkscrew "$proxy" "$proxyport" "$(bracketize_ipv6.py "$host")" "$port"
