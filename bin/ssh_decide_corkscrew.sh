#!/usr/bin/env zsh

set -o errexit -o nounset -o pipefail

# ssh itself fails with bracketed ipv6
host=$(echo "$1" | tr -d '[]')
port=$2
local_https_proxy=${https_proxy:-$default_https_proxy}
local_https_proxy=${local_https_proxy/http:\/\//}
proxyarr=("${(@s/:/)${local_https_proxy}}")
proxy=$proxyarr[1]
proxyport=$proxyarr[2]

(( ${+SSH_NO_PROXY} )) && echo 'direct (SSH_NO_PROXY)' >/dev/stderr && exit 1
(( ${+SSH_ALWAYS_PROXY} )) && echo 'via corkscrew (SSH_ALWAYS_PROXY)' >/dev/stderr && exit 0

if ! echo | nc -w 1 -z -q 0 "$host" "$port" 2>/dev/null && \
		echo | nc -w 1 -z -q 0 "$proxy" "$proxyport" 2>/dev/null; then
	echo via corkscrew >/dev/stderr
	exit 0
else
	exit 1
fi
