import unittest
from siportconv import reverse_siport_endian, convert


class TestSiportconv(unittest.TestCase):

    def test_reverse(self):
        self.assertEqual(reverse_siport_endian('043f49f24e7480'), '80744ef2493f04')
        self.assertEqual(reverse_siport_endian('80744ef2493f04'), '043f49f24e7480')

    def test_convert(self):
        self.assertEqual(convert('04:3f:49:f2:4e:74:80'), '80744EF2493F04')
        self.assertEqual(convert('80744EF2493F04'), '04:3f:49:f2:4e:74:80')
        with self.assertRaises(SystemExit):
            self.assertEqual(convert('80744EF2493F04FF'), '04:3f:49:f2:4e:74:80')
        with self.assertRaises(AssertionError):
            self.assertEqual(convert('80744EF2493F04'), '04:3f:49:f2:4e:74:70')

if __name__ == '__main__':
    unittest.main()
