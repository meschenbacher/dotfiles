#!/usr/bin/env python3

import sys
import netaddr
from netaddr import IPAddress

def bracketize_ipv6(ip: IPAddress):
    if ip.version == 4:
        return str(ip)
    else:
        return '[{}]'.format(str(ip))


def usage():
    print("Usage {} STRING".format(sys.argv[0]), file=sys.stderr)
    print("This script bracketizes a valid ipv6. All other inputs are output as given.",
            file=sys.stderr)
    sys.exit(1)

def main():
    if len(sys.argv[1:]) != 1:
        usage()

    host = sys.argv[1]
    try:
        ip = netaddr.IPAddress(host)
    except netaddr.core.AddrFormatError as e:
        # malformed ip addresses or regular hostnames may be submitted and we
        # transparently return them a such
        print(host)
        sys.exit(0)

    print(bracketize_ipv6(ip))

if __name__ == '__main__':
    main()
