#!/usr/bin/env python3

import sys

def reverse_siport_endian(string):
    """
    reverses a hex string in two pairs except for the 4th octet
    """
    assert len(string) % 2 == 0, "string length must be even"
    t = iter(string[::-1])
    out = ''
    i = 0
    for a, b in zip(t, t):
        if i == 3:
            out += a
            out += b
        else:
            out += b
            out += a
    return out

def convert(hexbytes):
    """
    converts a list of hexbytes from siport to dotted hexadecimal and vice versa
    The decision if siport or regular format is based on the byte 0x80 which comes first in
    siport and last in regular format.
    """
    string = hexbytes.replace(':', '')
    if len(string) != 14:
        print(string, "has incorrect length", len(string), file=sys.stderr)
        sys.exit(1)
    if string[:2] == "80":
        # siport to regular format
        s = reverse_siport_endian(string).lower()
        t = iter(s)
        return ':'.join(a+b for a,b in zip(t, t))
    elif string[-2:] == "80":
        # regular format to siport
        return reverse_siport_endian(string).upper()
    else:
        print("could not determine format (0x80 byte not found at start or end)",
                file=sys.stderr)
        sys.exit(1)

def read_argv():
    for arg in sys.argv[1:]:
        print(convert(arg))

def read_stdin():
    for arg in sys.stdin.read().split():
        print(convert(arg))

def main():
    if len(sys.argv) > 1:
        read_argv()
    else:
        read_stdin()

if __name__ == '__main__':
    main()
